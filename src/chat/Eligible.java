package chat;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class Eligible extends javax.swing.JFrame {

    public Eligible() {
        initComponents();
    }

  
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        INFO = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jButton28 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jButton30 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        jButton12 = new javax.swing.JButton();
        jButton13 = new javax.swing.JButton();
        jButton14 = new javax.swing.JButton();
        jButton15 = new javax.swing.JButton();
        jButton16 = new javax.swing.JButton();
        jButton17 = new javax.swing.JButton();
        jButton18 = new javax.swing.JButton();
        jButton19 = new javax.swing.JButton();
        jButton20 = new javax.swing.JButton();
        jButton21 = new javax.swing.JButton();
        jButton22 = new javax.swing.JButton();
        jButton23 = new javax.swing.JButton();
        jButton24 = new javax.swing.JButton();
        jButton25 = new javax.swing.JButton();
        jButton26 = new javax.swing.JButton();
        SUBSCRIBE = new javax.swing.JTextField();
        jButton27 = new javax.swing.JButton();

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        getContentPane().setLayout(null);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setLayout(null);

        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 51, 51));
        jLabel5.setText("   - Candidate can get admission according to MAH-MCA-CET score as PER DTE MCA-CET");
        jPanel3.add(jLabel5);
        jLabel5.setBounds(303, 82, 1190, 30);

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 51, 51));
        jLabel6.setText("     - ANY GRADUATION DEGREE(BSC IT,BSC-CS,BSC,BIO-TECH)");
        jPanel3.add(jLabel6);
        jLabel6.setBounds(290, 119, 1180, 50);

        INFO.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        INFO.setForeground(new java.awt.Color(0, 153, 102));
        INFO.setText("MAH-MCA-CET_2018-19_INFORMATION_BROCHURE");
        jPanel3.add(INFO);
        INFO.setBounds(336, 188, 1160, 40);

        jButton1.setBackground(new java.awt.Color(255, 255, 255));
        jButton1.setForeground(new java.awt.Color(0, 153, 0));
        jButton1.setText("BROCHURE.pdf");
        jButton1.setBorder(null);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton1);
        jButton1.setBounds(390, 260, 535, 42);

        jButton3.setBackground(new java.awt.Color(255, 255, 255));
        jButton3.setForeground(new java.awt.Color(0, 102, 255));
        jButton3.setText("Exam Dates");
        jButton3.setBorder(null);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton3);
        jButton3.setBounds(430, 330, 121, 15);

        jPanel1.setBackground(new java.awt.Color(255, 51, 51));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("MASTERS OF COMPUTER APPLICATIONS");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(449, 449, 449)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 458, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(593, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(13, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel3.add(jPanel1);
        jPanel1.setBounds(0, 0, 1500, 60);

        jButton28.setForeground(new java.awt.Color(0, 204, 102));
        jButton28.setText("Back");
        jButton28.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jButton28.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton28ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton28);
        jButton28.setBounds(800, 320, 94, 42);

        jPanel4.setBackground(new java.awt.Color(0, 51, 153));
        jPanel4.setForeground(new java.awt.Color(255, 255, 255));
        jPanel4.setLayout(null);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("SUBSCRIBE TO OUR CHANNEL");
        jPanel4.add(jLabel4);
        jLabel4.setBounds(410, 10, 630, 50);

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("TOP COLLEGES");
        jPanel4.add(jLabel11);
        jLabel11.setBounds(270, 130, 110, 17);

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("TOP UNIVERSITIES");
        jPanel4.add(jLabel12);
        jLabel12.setBounds(490, 130, 130, 20);

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("TOP EXAMS");
        jPanel4.add(jLabel13);
        jLabel13.setBounds(670, 130, 80, 17);

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("STUDY ABROAD");
        jPanel4.add(jLabel14);
        jLabel14.setBounds(850, 120, 120, 20);

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setText("ABOUT US");
        jPanel4.add(jLabel15);
        jLabel15.setBounds(1020, 120, 70, 20);

        jButton2.setBackground(new java.awt.Color(0, 51, 153));
        jButton2.setForeground(new java.awt.Color(255, 255, 255));
        jButton2.setText("M.B.A");
        jButton2.setBorder(null);
        jButton2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton2);
        jButton2.setBounds(280, 170, 100, 15);

        jButton30.setBackground(new java.awt.Color(0, 51, 153));
        jButton30.setForeground(new java.awt.Color(255, 255, 255));
        jButton30.setText("BTECH/B.E");
        jButton30.setBorder(null);
        jButton30.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton30.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton30ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton30);
        jButton30.setBounds(280, 200, 100, 15);

        jButton4.setBackground(new java.awt.Color(0, 51, 153));
        jButton4.setForeground(new java.awt.Color(255, 255, 255));
        jButton4.setText("B.C.A");
        jButton4.setBorder(null);
        jButton4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jPanel4.add(jButton4);
        jButton4.setBounds(290, 230, 90, 15);

        jButton5.setBackground(new java.awt.Color(0, 51, 153));
        jButton5.setForeground(new java.awt.Color(255, 255, 255));
        jButton5.setText("M.C.A");
        jButton5.setBorder(null);
        jButton5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton5);
        jButton5.setBounds(290, 260, 90, 15);

        jButton6.setBackground(new java.awt.Color(0, 51, 153));
        jButton6.setForeground(new java.awt.Color(255, 255, 255));
        jButton6.setText("M.TECH");
        jButton6.setBorder(null);
        jButton6.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton6);
        jButton6.setBounds(290, 290, 90, 15);

        jButton7.setBackground(new java.awt.Color(0, 51, 153));
        jButton7.setForeground(new java.awt.Color(255, 255, 255));
        jButton7.setText("ENGINEERING");
        jButton7.setBorder(null);
        jButton7.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton7);
        jButton7.setBounds(500, 170, 110, 15);

        jButton8.setBackground(new java.awt.Color(0, 51, 153));
        jButton8.setForeground(new java.awt.Color(255, 255, 255));
        jButton8.setText("MANAGEMENT");
        jButton8.setBorder(null);
        jButton8.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton8);
        jButton8.setBounds(500, 200, 110, 15);

        jButton9.setBackground(new java.awt.Color(0, 51, 153));
        jButton9.setForeground(new java.awt.Color(255, 255, 255));
        jButton9.setText("MEDICAL");
        jButton9.setBorder(null);
        jButton9.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton9);
        jButton9.setBounds(500, 230, 110, 15);

        jButton10.setBackground(new java.awt.Color(0, 51, 153));
        jButton10.setForeground(new java.awt.Color(255, 255, 255));
        jButton10.setText("LAW");
        jButton10.setBorder(null);
        jButton10.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton10);
        jButton10.setBounds(500, 260, 110, 15);

        jButton11.setBackground(new java.awt.Color(0, 51, 153));
        jButton11.setForeground(new java.awt.Color(255, 255, 255));
        jButton11.setText("SCIENCE");
        jButton11.setBorder(null);
        jButton11.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton11);
        jButton11.setBounds(500, 290, 110, 15);

        jButton12.setBackground(new java.awt.Color(0, 51, 153));
        jButton12.setForeground(new java.awt.Color(255, 255, 255));
        jButton12.setText("CAT");
        jButton12.setBorder(null);
        jButton12.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton12ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton12);
        jButton12.setBounds(680, 165, 80, 20);

        jButton13.setBackground(new java.awt.Color(0, 51, 153));
        jButton13.setForeground(new java.awt.Color(255, 255, 255));
        jButton13.setText("GATE");
        jButton13.setBorder(null);
        jButton13.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton13ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton13);
        jButton13.setBounds(680, 195, 80, 20);

        jButton14.setBackground(new java.awt.Color(0, 51, 153));
        jButton14.setForeground(new java.awt.Color(255, 255, 255));
        jButton14.setText("IIT-JEE");
        jButton14.setBorder(null);
        jButton14.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton14ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton14);
        jButton14.setBounds(680, 225, 80, 20);

        jButton15.setBackground(new java.awt.Color(0, 51, 153));
        jButton15.setForeground(new java.awt.Color(255, 255, 255));
        jButton15.setText("AIPMT");
        jButton15.setBorder(null);
        jButton15.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton15ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton15);
        jButton15.setBounds(680, 255, 90, 20);

        jButton16.setBackground(new java.awt.Color(0, 51, 153));
        jButton16.setForeground(new java.awt.Color(255, 255, 255));
        jButton16.setText("MAT");
        jButton16.setBorder(null);
        jButton16.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton16ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton16);
        jButton16.setBounds(680, 285, 90, 20);

        jButton17.setBackground(new java.awt.Color(0, 51, 153));
        jButton17.setForeground(new java.awt.Color(255, 255, 255));
        jButton17.setText("CANADA");
        jButton17.setBorder(null);
        jButton17.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton17ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton17);
        jButton17.setBounds(860, 160, 80, 15);

        jButton18.setBackground(new java.awt.Color(0, 51, 153));
        jButton18.setForeground(new java.awt.Color(255, 255, 255));
        jButton18.setText("U.S.A");
        jButton18.setBorder(null);
        jButton18.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton18ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton18);
        jButton18.setBounds(860, 190, 80, 15);

        jButton19.setBackground(new java.awt.Color(0, 51, 153));
        jButton19.setForeground(new java.awt.Color(255, 255, 255));
        jButton19.setText("GERMANY");
        jButton19.setBorder(null);
        jButton19.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton19ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton19);
        jButton19.setBounds(860, 220, 80, 15);

        jButton20.setBackground(new java.awt.Color(0, 51, 153));
        jButton20.setForeground(new java.awt.Color(255, 255, 255));
        jButton20.setText("U.K");
        jButton20.setBorder(null);
        jButton20.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton20ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton20);
        jButton20.setBounds(860, 250, 80, 15);

        jButton21.setBackground(new java.awt.Color(0, 51, 153));
        jButton21.setForeground(new java.awt.Color(255, 255, 255));
        jButton21.setText("SWEDEN");
        jButton21.setBorder(null);
        jButton21.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton21ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton21);
        jButton21.setBounds(860, 280, 80, 20);

        jButton22.setBackground(new java.awt.Color(0, 51, 153));
        jButton22.setForeground(new java.awt.Color(255, 255, 255));
        jButton22.setText("COLLEGE");
        jButton22.setBorder(null);
        jButton22.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton22ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton22);
        jButton22.setBounds(1020, 160, 100, 15);

        jButton23.setBackground(new java.awt.Color(0, 51, 153));
        jButton23.setForeground(new java.awt.Color(255, 255, 255));
        jButton23.setText("CONTACT US");
        jButton23.setBorder(null);
        jButton23.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jPanel4.add(jButton23);
        jButton23.setBounds(1020, 190, 100, 15);

        jButton24.setBackground(new java.awt.Color(0, 51, 153));
        jButton24.setForeground(new java.awt.Color(255, 255, 255));
        jButton24.setText("CAREER");
        jButton24.setBorder(null);
        jButton24.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton24.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton24ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton24);
        jButton24.setBounds(1020, 220, 90, 15);

        jButton25.setBackground(new java.awt.Color(0, 51, 153));
        jButton25.setForeground(new java.awt.Color(255, 255, 255));
        jButton25.setText("PRIVACY");
        jButton25.setBorder(null);
        jButton25.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jPanel4.add(jButton25);
        jButton25.setBounds(1020, 250, 100, 15);

        jButton26.setBackground(new java.awt.Color(0, 51, 153));
        jButton26.setForeground(new java.awt.Color(255, 255, 255));
        jButton26.setText("TERMS & CONDITION");
        jButton26.setBorder(null);
        jButton26.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jPanel4.add(jButton26);
        jButton26.setBounds(1020, 280, 150, 20);
        jPanel4.add(SUBSCRIBE);
        SUBSCRIBE.setBounds(500, 70, 290, 30);

        jButton27.setBackground(new java.awt.Color(0, 204, 255));
        jButton27.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jButton27.setForeground(new java.awt.Color(255, 255, 255));
        jButton27.setText("SUBMIT");
        jButton27.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton27.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton27ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton27);
        jButton27.setBounds(790, 60, 140, 50);

        jPanel3.add(jPanel4);
        jPanel4.setBounds(0, 400, 1490, 440);

        getContentPane().add(jPanel3);
        jPanel3.setBounds(0, 0, 1490, 770);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try { 
            Runtime.getRuntime().exec("rundll32 url.dll, FileProtocolHandler " + "C:\\Users\\madhu\\Desktop\\MAH-MCA-CET_2018-19_BROCHURE.pdf");
        } catch (IOException ex) {
            Logger.getLogger(Eligible.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
     new examdates().setVisible(true); 
     dispose();// TODO add your handling code here:
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton28ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton28ActionPerformed
        new mca().setVisible(true);       // TODO add your handling code here:
    }//GEN-LAST:event_jButton28ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        Desktop d=Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/mba-colleges"));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }           // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton30ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton30ActionPerformed
        Desktop d=Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/btech-colleges "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }          // TODO add your handling code here:
    }//GEN-LAST:event_jButton30ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        Desktop d=Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/mca-colleges "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        Desktop d=Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/mca-colleges "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }          // TODO add your handling code here:
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed

        Desktop d=Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/engineering-colleges   "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }    // TODO add your handling code here:
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed

        Desktop d=Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/management-colleges   "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        Desktop d=Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/medical-colleges   "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }      // TODO add your handling code here:
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        Desktop d=Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/law-colleges   "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }           // TODO add your handling code here:
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        Desktop d=Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/science-colleges   "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }           // TODO add your handling code here:
    }//GEN-LAST:event_jButton11ActionPerformed

    private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton12ActionPerformed
        Desktop d=Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/exams/cat  "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }           // TODO add your handling code here:
    }//GEN-LAST:event_jButton12ActionPerformed

    private void jButton13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton13ActionPerformed
        Desktop d=Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/exams/gate  "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }           // TODO add your handling code here:
    }//GEN-LAST:event_jButton13ActionPerformed

    private void jButton14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton14ActionPerformed
        Desktop d=Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/exams/jee-main   "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }           // TODO add your handling code here:
    }//GEN-LAST:event_jButton14ActionPerformed

    private void jButton15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton15ActionPerformed
        Desktop d=Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/exams/neet  "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }           // TODO add your handling code here:
    }//GEN-LAST:event_jButton15ActionPerformed

    private void jButton16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton16ActionPerformed
        Desktop d=Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/exams/mat  "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }         // TODO add your handling code here:
    }//GEN-LAST:event_jButton16ActionPerformed

    private void jButton17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton17ActionPerformed
        Desktop d=Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/canada  "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }           // TODO add your handling code here:
    }//GEN-LAST:event_jButton17ActionPerformed

    private void jButton18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton18ActionPerformed
        Desktop d=Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/usa  "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }           // TODO add your handling code here:
    }//GEN-LAST:event_jButton18ActionPerformed

    private void jButton19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton19ActionPerformed
        Desktop d=Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/germany"));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }           // TODO add your handling code here:
    }//GEN-LAST:event_jButton19ActionPerformed

    private void jButton20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton20ActionPerformed
        Desktop d=Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/uk  "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }           // TODO add your handling code here:
    }//GEN-LAST:event_jButton20ActionPerformed

    private void jButton21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton21ActionPerformed
        Desktop d=Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/sweden "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }           // TODO add your handling code here:
    }//GEN-LAST:event_jButton21ActionPerformed

    private void jButton22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton22ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton22ActionPerformed

    private void jButton24ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton24ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton24ActionPerformed

    private void jButton27ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton27ActionPerformed
        try{

            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/db","root","root");
            String query="INSERT INTO subscribe(SUBSCRIBE)values(?);";
            PreparedStatement ps=conn.prepareStatement(query);
            if (SUBSCRIBE.getText().length()==0)
            JOptionPane.showMessageDialog(null,"PLEASE MENTION YOUR EMAIL-ID TO SUBSCRIBE OUR CHANNEL!!!");
            else{
                ps.setString(1,SUBSCRIBE.getText());
                ps.executeUpdate();
                JOptionPane.showMessageDialog(null,"THANK YOU FOR SUBSCRIBING OUR CHANNEL!!!");
                conn.close();
            }
        
            // TODO add your handling code here:
    }//GEN-LAST:event_jButton27ActionPerformed
catch(ClassNotFoundException | SQLException e)
               {
                    JOptionPane.showMessageDialog(null,e);
               }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Eligible.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Eligible.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Eligible.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Eligible.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new Eligible().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel INFO;
    private javax.swing.JTextField SUBSCRIBE;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton13;
    private javax.swing.JButton jButton14;
    private javax.swing.JButton jButton15;
    private javax.swing.JButton jButton16;
    private javax.swing.JButton jButton17;
    private javax.swing.JButton jButton18;
    private javax.swing.JButton jButton19;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton20;
    private javax.swing.JButton jButton21;
    private javax.swing.JButton jButton22;
    private javax.swing.JButton jButton23;
    private javax.swing.JButton jButton24;
    private javax.swing.JButton jButton25;
    private javax.swing.JButton jButton26;
    private javax.swing.JButton jButton27;
    private javax.swing.JButton jButton28;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton30;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    // End of variables declaration//GEN-END:variables
}
