package chat;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class PLACECOMM extends javax.swing.JFrame {

    public PLACECOMM() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        name = new javax.swing.JTextField();
        city = new javax.swing.JTextField();
        phn = new javax.swing.JTextField();
        job = new javax.swing.JTextField();
        dob = new com.toedter.calendar.JDateChooser();
        per = new javax.swing.JTextField();
        email = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jButton28 = new javax.swing.JButton();
        grad = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        jButton12 = new javax.swing.JButton();
        jButton13 = new javax.swing.JButton();
        jButton14 = new javax.swing.JButton();
        jButton15 = new javax.swing.JButton();
        jButton16 = new javax.swing.JButton();
        jButton17 = new javax.swing.JButton();
        jButton18 = new javax.swing.JButton();
        jButton19 = new javax.swing.JButton();
        jButton20 = new javax.swing.JButton();
        jButton21 = new javax.swing.JButton();
        jButton22 = new javax.swing.JButton();
        jButton23 = new javax.swing.JButton();
        jButton24 = new javax.swing.JButton();
        jButton25 = new javax.swing.JButton();
        jButton26 = new javax.swing.JButton();
        SUBSCRIBE = new javax.swing.JTextField();
        jButton27 = new javax.swing.JButton();

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(800, 900));
        getContentPane().setLayout(null);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(null);

        jLabel2.setText("NAME:");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(90, 100, 32, 14);

        jLabel3.setText("DATE OF BIRTH:");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(70, 150, 110, 14);

        jLabel5.setText("CITY/STATE/ZIP:");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(70, 190, 90, 10);

        jLabel6.setText("CONTACT NO:");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(70, 220, 70, 20);

        jLabel7.setText("JOB TITLE:");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(80, 260, 80, 14);

        jLabel8.setText("GRADUATION/POST GRADUATION:");
        jPanel1.add(jLabel8);
        jLabel8.setBounds(50, 310, 180, 14);

        jLabel9.setText("PERCENTAGE/CGPA:");
        jPanel1.add(jLabel9);
        jLabel9.setBounds(370, 310, 110, 14);

        jLabel10.setText("EMAIL-ID:");
        jPanel1.add(jLabel10);
        jLabel10.setBounds(80, 360, 60, 14);

        jButton1.setBackground(new java.awt.Color(0, 153, 255));
        jButton1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setText("SUBMIT");
        jButton1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);
        jButton1.setBounds(120, 400, 120, 30);
        jPanel1.add(name);
        name.setBounds(300, 100, 190, 20);
        jPanel1.add(city);
        city.setBounds(300, 180, 190, 20);
        jPanel1.add(phn);
        phn.setBounds(300, 220, 190, 20);
        jPanel1.add(job);
        job.setBounds(300, 260, 190, 20);
        jPanel1.add(dob);
        dob.setBounds(300, 140, 190, 20);
        jPanel1.add(per);
        per.setBounds(490, 310, 110, 20);
        jPanel1.add(email);
        email.setBounds(300, 360, 190, 20);

        jPanel4.setBackground(new java.awt.Color(102, 153, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 102, 51));
        jLabel1.setText("PLACECOMM REGISTRATION FORM 2019");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(68, 68, 68)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1037, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(405, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(28, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(23, 23, 23))
        );

        jPanel1.add(jPanel4);
        jPanel4.setBounds(-20, -10, 1510, 80);

        jButton28.setForeground(new java.awt.Color(0, 204, 102));
        jButton28.setText("Back");
        jButton28.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jButton28.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton28.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton28ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton28);
        jButton28.setBounds(300, 390, 94, 42);
        jPanel1.add(grad);
        grad.setBounds(250, 310, 90, 20);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 1500, 450);

        jPanel3.setBackground(new java.awt.Color(0, 51, 153));
        jPanel3.setForeground(new java.awt.Color(255, 255, 255));
        jPanel3.setLayout(null);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("SUBSCRIBE TO OUR CHANNEL");
        jPanel3.add(jLabel4);
        jLabel4.setBounds(100, 4, 630, 50);

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("TOP COLLEGES");
        jPanel3.add(jLabel11);
        jLabel11.setBounds(10, 120, 110, 17);

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("TOP UNIVERSITIES");
        jPanel3.add(jLabel12);
        jLabel12.setBounds(170, 120, 130, 20);

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("TOP EXAMS");
        jPanel3.add(jLabel13);
        jLabel13.setBounds(340, 120, 80, 17);

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("STUDY ABROAD");
        jPanel3.add(jLabel14);
        jLabel14.setBounds(470, 120, 120, 20);

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setText("ABOUT US");
        jPanel3.add(jLabel15);
        jLabel15.setBounds(640, 120, 70, 20);

        jButton2.setBackground(new java.awt.Color(0, 51, 153));
        jButton2.setForeground(new java.awt.Color(255, 255, 255));
        jButton2.setText("M.B.A");
        jButton2.setBorder(null);
        jButton2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton2);
        jButton2.setBounds(20, 160, 100, 15);

        jButton3.setBackground(new java.awt.Color(0, 51, 153));
        jButton3.setForeground(new java.awt.Color(255, 255, 255));
        jButton3.setText("BTECH/B.E");
        jButton3.setBorder(null);
        jButton3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton3);
        jButton3.setBounds(20, 190, 100, 15);

        jButton4.setBackground(new java.awt.Color(0, 51, 153));
        jButton4.setForeground(new java.awt.Color(255, 255, 255));
        jButton4.setText("B.C.A");
        jButton4.setBorder(null);
        jButton4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jPanel3.add(jButton4);
        jButton4.setBounds(20, 220, 90, 15);

        jButton5.setBackground(new java.awt.Color(0, 51, 153));
        jButton5.setForeground(new java.awt.Color(255, 255, 255));
        jButton5.setText("M.C.A");
        jButton5.setBorder(null);
        jButton5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton5);
        jButton5.setBounds(20, 250, 90, 15);

        jButton6.setBackground(new java.awt.Color(0, 51, 153));
        jButton6.setForeground(new java.awt.Color(255, 255, 255));
        jButton6.setText("M.TECH");
        jButton6.setBorder(null);
        jButton6.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton6);
        jButton6.setBounds(20, 280, 90, 15);

        jButton7.setBackground(new java.awt.Color(0, 51, 153));
        jButton7.setForeground(new java.awt.Color(255, 255, 255));
        jButton7.setText("ENGINEERING");
        jButton7.setBorder(null);
        jButton7.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton7);
        jButton7.setBounds(180, 160, 110, 15);

        jButton8.setBackground(new java.awt.Color(0, 51, 153));
        jButton8.setForeground(new java.awt.Color(255, 255, 255));
        jButton8.setText("MANAGEMENT");
        jButton8.setBorder(null);
        jButton8.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton8);
        jButton8.setBounds(180, 190, 110, 15);

        jButton9.setBackground(new java.awt.Color(0, 51, 153));
        jButton9.setForeground(new java.awt.Color(255, 255, 255));
        jButton9.setText("MEDICAL");
        jButton9.setBorder(null);
        jButton9.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton9);
        jButton9.setBounds(180, 220, 110, 15);

        jButton10.setBackground(new java.awt.Color(0, 51, 153));
        jButton10.setForeground(new java.awt.Color(255, 255, 255));
        jButton10.setText("LAW");
        jButton10.setBorder(null);
        jButton10.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton10);
        jButton10.setBounds(180, 250, 110, 15);

        jButton11.setBackground(new java.awt.Color(0, 51, 153));
        jButton11.setForeground(new java.awt.Color(255, 255, 255));
        jButton11.setText("SCIENCE");
        jButton11.setBorder(null);
        jButton11.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton11);
        jButton11.setBounds(180, 280, 110, 15);

        jButton12.setBackground(new java.awt.Color(0, 51, 153));
        jButton12.setForeground(new java.awt.Color(255, 255, 255));
        jButton12.setText("CAT");
        jButton12.setBorder(null);
        jButton12.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton12ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton12);
        jButton12.setBounds(340, 160, 80, 15);

        jButton13.setBackground(new java.awt.Color(0, 51, 153));
        jButton13.setForeground(new java.awt.Color(255, 255, 255));
        jButton13.setText("GATE");
        jButton13.setBorder(null);
        jButton13.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton13ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton13);
        jButton13.setBounds(340, 190, 80, 15);

        jButton14.setBackground(new java.awt.Color(0, 51, 153));
        jButton14.setForeground(new java.awt.Color(255, 255, 255));
        jButton14.setText("IIT-JEE");
        jButton14.setBorder(null);
        jButton14.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton14ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton14);
        jButton14.setBounds(340, 220, 80, 15);

        jButton15.setBackground(new java.awt.Color(0, 51, 153));
        jButton15.setForeground(new java.awt.Color(255, 255, 255));
        jButton15.setText("AIPMT");
        jButton15.setBorder(null);
        jButton15.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton15ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton15);
        jButton15.setBounds(340, 250, 90, 15);

        jButton16.setBackground(new java.awt.Color(0, 51, 153));
        jButton16.setForeground(new java.awt.Color(255, 255, 255));
        jButton16.setText("MAT");
        jButton16.setBorder(null);
        jButton16.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton16ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton16);
        jButton16.setBounds(340, 280, 90, 15);

        jButton17.setBackground(new java.awt.Color(0, 51, 153));
        jButton17.setForeground(new java.awt.Color(255, 255, 255));
        jButton17.setText("CANADA");
        jButton17.setBorder(null);
        jButton17.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton17ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton17);
        jButton17.setBounds(480, 160, 80, 15);

        jButton18.setBackground(new java.awt.Color(0, 51, 153));
        jButton18.setForeground(new java.awt.Color(255, 255, 255));
        jButton18.setText("U.S.A");
        jButton18.setBorder(null);
        jButton18.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton18ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton18);
        jButton18.setBounds(480, 190, 80, 15);

        jButton19.setBackground(new java.awt.Color(0, 51, 153));
        jButton19.setForeground(new java.awt.Color(255, 255, 255));
        jButton19.setText("GERMANY");
        jButton19.setBorder(null);
        jButton19.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton19ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton19);
        jButton19.setBounds(480, 220, 80, 15);

        jButton20.setBackground(new java.awt.Color(0, 51, 153));
        jButton20.setForeground(new java.awt.Color(255, 255, 255));
        jButton20.setText("U.K");
        jButton20.setBorder(null);
        jButton20.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton20ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton20);
        jButton20.setBounds(480, 250, 80, 15);

        jButton21.setBackground(new java.awt.Color(0, 51, 153));
        jButton21.setForeground(new java.awt.Color(255, 255, 255));
        jButton21.setText("SWEDEN");
        jButton21.setBorder(null);
        jButton21.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton21ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton21);
        jButton21.setBounds(480, 280, 80, 15);

        jButton22.setBackground(new java.awt.Color(0, 51, 153));
        jButton22.setForeground(new java.awt.Color(255, 255, 255));
        jButton22.setText("COLLEGE");
        jButton22.setBorder(null);
        jButton22.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton22ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton22);
        jButton22.setBounds(630, 160, 100, 15);

        jButton23.setBackground(new java.awt.Color(0, 51, 153));
        jButton23.setForeground(new java.awt.Color(255, 255, 255));
        jButton23.setText("CONTACT US");
        jButton23.setBorder(null);
        jButton23.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jPanel3.add(jButton23);
        jButton23.setBounds(630, 190, 100, 15);

        jButton24.setBackground(new java.awt.Color(0, 51, 153));
        jButton24.setForeground(new java.awt.Color(255, 255, 255));
        jButton24.setText("CAREER");
        jButton24.setBorder(null);
        jButton24.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton24.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton24ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton24);
        jButton24.setBounds(630, 220, 100, 15);

        jButton25.setBackground(new java.awt.Color(0, 51, 153));
        jButton25.setForeground(new java.awt.Color(255, 255, 255));
        jButton25.setText("PRIVACY");
        jButton25.setBorder(null);
        jButton25.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jPanel3.add(jButton25);
        jButton25.setBounds(630, 250, 100, 15);

        jButton26.setBackground(new java.awt.Color(0, 51, 153));
        jButton26.setForeground(new java.awt.Color(255, 255, 255));
        jButton26.setText("TERMS & CONDITION");
        jButton26.setBorder(null);
        jButton26.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jPanel3.add(jButton26);
        jButton26.setBounds(620, 280, 150, 20);
        jPanel3.add(SUBSCRIBE);
        SUBSCRIBE.setBounds(120, 50, 290, 30);

        jButton27.setBackground(new java.awt.Color(0, 204, 255));
        jButton27.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jButton27.setForeground(new java.awt.Color(255, 255, 255));
        jButton27.setText("SUBMIT");
        jButton27.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton27.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton27ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton27);
        jButton27.setBounds(410, 50, 140, 40);

        getContentPane().add(jPanel3);
        jPanel3.setBounds(0, 450, 1490, 390);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {

            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/db", "root", "root");
            String query = "INSERT INTO placecom(name,dob,city,phn,job,grad,per,email)values(?,?,?,?,?,?,?,?);";
            // String query2="delete from issuebooks where isbn_no=? and sid=?";
            PreparedStatement ps = conn.prepareStatement(query);
            //   PreparedStatement pst=conn.prepareStatement(query2); 
            // ps.setInt(1,Integer.parseInt(id.getText()));
            if (name.getText().length() == 0) {
                JOptionPane.showMessageDialog(null, "Empty Fields Detected, Please fill up all fields!!!");
            } else if (city.getText().length() == 0) {
                JOptionPane.showMessageDialog(null, "Empty Fields Detected, Please fill up all fields!!!");
            } else if (phn.getText().length() == 0) {
                JOptionPane.showMessageDialog(null, "Empty Fields Detected, Please fill up all fields!!!");
            } else if (job.getText().length() == 0) {
                JOptionPane.showMessageDialog(null, "Empty Fields Detected, Please fill up all fields!!!");
            } else if (per.getText().length() == 0) {
                JOptionPane.showMessageDialog(null, "Empty Fields Detected, Please fill up all fields!!!");
            } else if (email.getText().length() == 0) {
                JOptionPane.showMessageDialog(null, "Empty Fields Detected, Please fill up all fields!!!");
            } else if (grad.getText().length() == 0) {
                JOptionPane.showMessageDialog(null, "Empty Fields Detected, Please fill up all fields!!!");
            } else {

                ps.setString(1, name.getText());
                ps.setDate(2, new java.sql.Date(dob.getDate().getTime()));
                ps.setString(3, city.getText());
                ps.setInt(4, Integer.parseInt(phn.getText()));
                ps.setString(5, job.getText());
                ps.setString(6, grad.getText());
                ps.setInt(7, Integer.parseInt(per.getText()));
                ps.setString(8, email.getText());
                //  ps.setInt(9,0);
                ps.executeUpdate();
                JOptionPane.showMessageDialog(null, "Registered Successfully");
                //    pst.setInt(1,Integer.parseInt(isbno.getText()));
                //   SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
                //   String date=sdf.format(added_date.getDate());
                // ps.setString(7, date);  
                //    pst.executeUpdate();
                //  JOptionPane.showMessageDialog(null,"deleted succesfull");
                conn.close();
            }
        } catch (ClassNotFoundException | SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }

        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton24ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton24ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton24ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        Desktop d = Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/mba-colleges"));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }           // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        Desktop d = Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/btech-colleges "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }          // TODO add your handling code here:
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        Desktop d = Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/mca-colleges "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        Desktop d = Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/mca-colleges "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }          // TODO add your handling code here:
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed

        Desktop d = Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/engineering-colleges   "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }    // TODO add your handling code here:
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed

        Desktop d = Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/management-colleges   "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }
// TODO add your handling code here:
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        Desktop d = Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/medical-colleges   "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }      // TODO add your handling code here:
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        Desktop d = Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/law-colleges   "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }           // TODO add your handling code here:
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        Desktop d = Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/science-colleges   "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }           // TODO add your handling code here:
    }//GEN-LAST:event_jButton11ActionPerformed

    private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton12ActionPerformed
        Desktop d = Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/exams/cat  "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }           // TODO add your handling code here:
    }//GEN-LAST:event_jButton12ActionPerformed

    private void jButton13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton13ActionPerformed
        Desktop d = Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/exams/gate  "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }           // TODO add your handling code here:
    }//GEN-LAST:event_jButton13ActionPerformed

    private void jButton14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton14ActionPerformed
        Desktop d = Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/exams/jee-main   "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }           // TODO add your handling code here:
    }//GEN-LAST:event_jButton14ActionPerformed

    private void jButton15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton15ActionPerformed
        Desktop d = Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/exams/neet  "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }           // TODO add your handling code here:
    }//GEN-LAST:event_jButton15ActionPerformed

    private void jButton16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton16ActionPerformed
        Desktop d = Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/exams/mat  "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }         // TODO add your handling code here:
    }//GEN-LAST:event_jButton16ActionPerformed

    private void jButton17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton17ActionPerformed
        Desktop d = Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/canada  "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }           // TODO add your handling code here:
    }//GEN-LAST:event_jButton17ActionPerformed

    private void jButton18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton18ActionPerformed
        Desktop d = Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/usa  "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }           // TODO add your handling code here:
    }//GEN-LAST:event_jButton18ActionPerformed

    private void jButton19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton19ActionPerformed
        Desktop d = Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/germany"));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }           // TODO add your handling code here:
    }//GEN-LAST:event_jButton19ActionPerformed

    private void jButton20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton20ActionPerformed
        Desktop d = Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/uk  "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }           // TODO add your handling code here:
    }//GEN-LAST:event_jButton20ActionPerformed

    private void jButton21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton21ActionPerformed
        Desktop d = Desktop.getDesktop();
        try {
            d.browse(new URI("https://collegedunia.com/sweden "));         // TODO add your handling code here:
        } catch (IOException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(examdates.class.getName()).log(Level.SEVERE, null, ex);

        }           // TODO add your handling code here:
    }//GEN-LAST:event_jButton21ActionPerformed

    private void jButton22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton22ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton22ActionPerformed

    private void jButton28ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton28ActionPerformed
        new mca().setVisible(true);       // TODO add your handling code here:
    }//GEN-LAST:event_jButton28ActionPerformed

    private void jButton27ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton27ActionPerformed
        try {

            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/db", "root", "root");
            String query = "INSERT INTO subscribe(SUBSCRIBE)values(?);";
            PreparedStatement ps = conn.prepareStatement(query);
            if (SUBSCRIBE.getText().length() == 0) {
                JOptionPane.showMessageDialog(null, "PLEASE MENTION YOUR EMAIL-ID TO SUBSCRIBE OUR CHANNEL!!!");
            } else {
                ps.setString(1, SUBSCRIBE.getText());
                ps.executeUpdate();
                JOptionPane.showMessageDialog(null, "THANK YOU FOR SUBSCRIBING OUR CHANNEL!!!");
                conn.close();
            }
// TODO add your handling code here:
    }//GEN-LAST:event_jButton27ActionPerformed
 catch (ClassNotFoundException | SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PLACECOMM().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField SUBSCRIBE;
    private javax.swing.JTextField city;
    private com.toedter.calendar.JDateChooser dob;
    private javax.swing.JTextField email;
    private javax.swing.JTextField grad;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton13;
    private javax.swing.JButton jButton14;
    private javax.swing.JButton jButton15;
    private javax.swing.JButton jButton16;
    private javax.swing.JButton jButton17;
    private javax.swing.JButton jButton18;
    private javax.swing.JButton jButton19;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton20;
    private javax.swing.JButton jButton21;
    private javax.swing.JButton jButton22;
    private javax.swing.JButton jButton23;
    private javax.swing.JButton jButton24;
    private javax.swing.JButton jButton25;
    private javax.swing.JButton jButton26;
    private javax.swing.JButton jButton27;
    private javax.swing.JButton jButton28;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JTextField job;
    private javax.swing.JTextField name;
    private javax.swing.JTextField per;
    private javax.swing.JTextField phn;
    // End of variables declaration//GEN-END:variables
}
