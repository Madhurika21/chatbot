package chat;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class admin extends javax.swing.JFrame {

    public admin() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        USERNAME = new javax.swing.JTextField();
        USERTYPE = new javax.swing.JComboBox<>();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        PASSWORD = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        setForeground(new java.awt.Color(0, 204, 0));
        setMinimumSize(new java.awt.Dimension(100, 100));
        setSize(new java.awt.Dimension(700, 500));
        getContentPane().setLayout(null);

        jPanel1.setBackground(new java.awt.Color(51, 204, 255));
        jPanel1.setLayout(null);

        jButton3.setBackground(new java.awt.Color(255, 102, 0));
        jButton3.setForeground(new java.awt.Color(255, 255, 255));
        jButton3.setText("SIGNUP/REGISTER");
        jButton3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jButton3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3);
        jButton3.setBounds(220, 170, 330, 60);

        jButton4.setBackground(new java.awt.Color(255, 255, 51));
        jButton4.setForeground(new java.awt.Color(255, 255, 255));
        jButton4.setText("LOGIN");
        jButton4.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jButton4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton4);
        jButton4.setBounds(220, 260, 330, 60);

        jPanel4.setBackground(new java.awt.Color(51, 204, 255));
        jPanel4.setLayout(null);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("SIGNUP YOUR ACCOUNT");
        jPanel4.add(jLabel5);
        jLabel5.setBounds(62, 18, 330, 80);

        jPanel1.add(jPanel4);
        jPanel4.setBounds(0, 0, 430, 0);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(-10, 0, 690, 650);

        jPanel2.setBackground(new java.awt.Color(51, 0, 102));
        jPanel2.setLayout(null);

        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("USER:");
        jPanel2.add(jLabel1);
        jLabel1.setBounds(221, 120, 100, 33);

        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("PASSWORD:");
        jPanel2.add(jLabel2);
        jLabel2.setBounds(219, 170, 80, 27);

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("USERTYPE:");
        jPanel2.add(jLabel3);
        jLabel3.setBounds(230, 240, 81, 25);
        jPanel2.add(USERNAME);
        USERNAME.setBounds(370, 120, 215, 20);

        USERTYPE.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ADMIN", "USER", " " }));
        USERTYPE.setPreferredSize(new java.awt.Dimension(58, 30));
        jPanel2.add(USERTYPE);
        USERTYPE.setBounds(370, 230, 100, 30);

        jButton1.setText("SUBMIT");
        jButton1.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        jButton1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton1);
        jButton1.setBounds(229, 330, 120, 30);

        jButton2.setText("CANCEL");
        jButton2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jButton2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jPanel2.add(jButton2);
        jButton2.setBounds(410, 330, 90, 30);

        jPanel3.setBackground(new java.awt.Color(255, 204, 0));
        jPanel3.setLayout(null);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("LOGIN FORM");
        jPanel3.add(jLabel4);
        jLabel4.setBounds(22, 38, 158, 29);

        jPanel2.add(jPanel3);
        jPanel3.setBounds(0, 0, 400, 0);
        jPanel2.add(PASSWORD);
        PASSWORD.setBounds(370, 170, 210, 20);

        getContentPane().add(jPanel2);
        jPanel2.setBounds(680, 0, 790, 650);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
    int i= USERTYPE.getSelectedIndex();
   if(i==0)
{   
     PreparedStatement ps;
       ResultSet rs;
       String uname=USERNAME.getText();
        String pass=String.valueOf(PASSWORD.getPassword()); 
       String query="SELECT * FROM MULTIUSERLOGIN WHERE USERNAME=? AND PASSWORD=? AND USERTYPE=?";
        try {
            
            Class.forName("com.mysql.jdbc.Driver");
        
           Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/db","root","root");  
      ps=conn.prepareStatement(query);
      ps.setString(1, uname);
      ps.setString(2, pass);
       ps.setString(3, String.valueOf(USERTYPE.getSelectedItem()));
         rs=ps.executeQuery();
         if(rs.next())
         {
             JOptionPane.showMessageDialog(null,"USER LOGGED IN!!!!"+ rs.getString("USERTYPE"));
             //if(USERTYPE.getSelectedIndex()==0){
              INVALIDANS  a=new INVALIDANS();
                  a.setVisible(true);
                 //this.setVisible(false); 
             dispose();
             }
          else{
                   JOptionPane.showMessageDialog(this,"INVALID USERNAME AND PASSWORD!!!");    
                     }
} catch(Exception e)
{
    System.out.println(e);
}}
else if(i==1)
{
        
      
           PreparedStatement ps;
       ResultSet rs;
       String uname=USERNAME.getText();
        String pass=String.valueOf(PASSWORD.getPassword()); 
       String query="SELECT * FROM regist WHERE user=? AND pwd=?";
       try {
            Class.forName("com.mysql.jdbc.Driver");
        
           Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/db","root","root");  
      ps=conn.prepareStatement(query);
      ps.setString(1, uname);
      ps.setString(2, pass);
      
    //    ps= conn.getConnection().prepareStatement(query);
         rs=ps.executeQuery();
         if(rs.next())
         {
             JOptionPane.showMessageDialog(null,"USER LOGGED IN!!!!");
             new chatbotai().setVisible(true); 
             dispose();
             
         }
         else{
             JOptionPane.showMessageDialog(null,"INVALID LOGIN"); 
            
         }
        }

        catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Loginchatbot.class.getName()).log(Level.SEVERE, null, ex);
        }
}


     
        
        
        
        
        
        
        /*int i= USERTYPE.getSelectedIndex();
      
     PreparedStatement ps;
       ResultSet rs;
       String uname=USERNAME.getText();
        String pass=String.valueOf(PASSWORD.getPassword()); 
       String query="SELECT * FROM MULTIUSERLOGIN WHERE USERNAME=? AND PASSWORD=? AND USERTYPE=?";
        try {
            
            Class.forName("com.mysql.jdbc.Driver");
        
           Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/db","root","root");  
      ps=conn.prepareStatement(query);
      ps.setString(1, uname);
      ps.setString(2, pass);
       ps.setString(3, String.valueOf(USERTYPE.getSelectedItem()));
         rs=ps.executeQuery();
         if(rs.next())
         {
             JOptionPane.showMessageDialog(null,"USER LOGGED IN!!!!"+ rs.getString("USERTYPE"));
             if(USERTYPE.getSelectedIndex()==0){
              INVALIDANS  a=new INVALIDANS();
                  a.setVisible(true);
                 //this.setVisible(false); 
             dispose();
             }
         else{
       chatbotai u=new chatbotai();
          u.setVisible(true);
          dispose();
          // this.setVisible(false);
             }
         }
             else{
                   JOptionPane.showMessageDialog(this,"INVALID USERNAME AND PASSWORD!!!");    
                     }
         
    }
    
        catch (ClassNotFoundException | SQLException ex) {
          JOptionPane.showMessageDialog(this,ex.getMessage());    
        
        
        }*/// TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
new reg().setVisible(true); 
dispose();// TODO add your handling code here:
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
new admin().setVisible(true);  
dispose();// TODO add your handling code here:
    }//GEN-LAST:event_jButton4ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
         java.awt.EventQueue.invokeLater(() -> {
           new admin().setVisible(true);
        });

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPasswordField PASSWORD;
    private javax.swing.JTextField USERNAME;
    private javax.swing.JComboBox<String> USERTYPE;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    // End of variables declaration//GEN-END:variables
}
